'''
Shared library
'''

import os
import pandas as pd
import numpy as np
import category_encoders as ce

from sklearn.utils import class_weight
from sklearn.model_selection import StratifiedKFold

import logging
import copy
from CsvColumnMetadata import  CsvColumnMetadata, \
    CsvColumnType
from sklearn import preprocessing

DIAGNOSTIC = True
EXPLAIN_DIAG = True


def calcStats(df, optNumericCols = None):
    '''
    Calculate statistics of a Pandas DF: mean, std, var, min, max, diff, norm_range
    return a dataframe containing statistics
    '''
    numericCols = df.columns if optNumericCols is None else optNumericCols
    # colAttribDf (column attributes Df), i.e. mean, std, etc of all cols
    colAttribDf = pd.DataFrame()
    colAttribDf["mean"] = df[numericCols].mean()
    colAttribDf["std"] = df[numericCols].std()
    colAttribDf["var"] = df[numericCols].var()
    colAttribDf["min"] = df[numericCols].min()
    colAttribDf["max"] = df[numericCols].max()
    colAttribDf["diff"] = df[numericCols].max() - df[numericCols].min()
    colAttribDf["norm_range"] = 0

    # Calc norm_range for all cols (including outcome)
    for feature_name in numericCols:
        max_value = colAttribDf.loc[feature_name, 'max']
        min_value = colAttribDf.loc[feature_name, 'min']
        norm_range = max_value - min_value
        if norm_range == 0:
            norm_range = 1
        colAttribDf.loc[[feature_name], ["norm_range"]] = norm_range
    return colAttribDf


def normalizeDfCols(df, colNameList):
    '''
    Normalize selected df cols
    '''
    x = df[colNameList].values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    scaledDf = pd.DataFrame(x_scaled,
                           columns = colNameList,
                           index = df.index)
    return scaledDf


def appendRowToDf(df, r):
    df.loc[len(df.index)] = r


def read_data_generic(
        inputCsvFilename,
        csvMetadata,
        trim_stdev,  # trim using mean plus minus (this number x stdev)
        optRowMowMax = None,  # number of maximum sample
#         acceptFewerRows = False,
        skipOnehotEncoding = False,
        skipNormalizing = False,
        optCustomStdevTrimFeaturesList = None,  # custom numeric features to be trimmed, default is trim all
        optOutputDir = None,  # output folder, if diagnostics
        diag = False
        ):
    df = load_data_generic(inputCsvFilename)

    return process_data_generic(
        df,
        csvMetadata,
        trim_stdev,  # trim using mean plus minus (this number x stdev)
        optRowMowMax,  # number of maximum sample
#         acceptFewerRows = acceptFewerRows,
        skipOnehotEncoding = skipOnehotEncoding,
        skipNormalizing = skipNormalizing,
        optCustomStdevTrimFeaturesList = optCustomStdevTrimFeaturesList,  # custom numeric features to be trimmed, default is trim all
        optOutputDir = optOutputDir,  # output folder, if diagnostics
        diag = diag)


def load_data_generic(
        inputCsvFilename,
        ):
    df = pd.read_csv(inputCsvFilename)
    return df


def process_data_generic(
        df,
        csvMetadata,
        trim_stdev,  # trim using mean plus minus (this number x stdev)
        optRowMowMax = None,  # number of maximum sample
#         acceptFewerRows = False,
        skipOnehotEncoding = False,
        skipNormalizing = False,
        optCustomStdevTrimFeaturesList = None,  # custom numeric features to be trimmed, default is trim all
        optOutputDir = None,  # output folder, if diagnostics
        diag = False
        ):
    '''
    Read up to rowMax randomly selected rows of data from a csv file
    return Pandas DF and resultant column metadata list (after any one hot column additions)
    '''
    # df['Outcome'] = np.where(df['Outcome_Default'] == True, 1, 0)

    # convert categorical to numeric
    initialColNameSet = set(copy.deepcopy(df.columns))

    # We only consider specified columns - all others are
    # dropped. For one-hotted columns, all one hotted
    # columns calculated from any CATEGORICAL column
    # across all data are also included. For data already
    # one hotted, the one hotted columns would be defined
    # as CATEGORICAL and isOrdinal is True, in which case
    # those columns would be included like any other.
    featureColsToDrop = initialColNameSet - \
        initialColNameSet.intersection(csvMetadata.getFeatureNameList()) - \
        set(csvMetadata.getOutputColumnNameList())
    df = df.drop(list(featureColsToDrop), axis = 1)

    # Drop all columns not in initialColNameSet

    oneHotNameList = csvMetadata.getOneHotNameList()

    # Initially (ignoring the one hot columns, filter out)
    # all of the columns not in specified in the metadata

    ce_one_hot = ce.OneHotEncoder(cols = oneHotNameList)
    df = ce_one_hot.fit_transform(df)
    ordinalNameList = csvMetadata.getOrdinalNameList()
    ce_ordinal = ce.OrdinalEncoder(cols = ordinalNameList)
    df = ce_ordinal.fit_transform(df)

    derivedCsvMetadata = copy.deepcopy(csvMetadata)
    if not skipOnehotEncoding:
        # All one-hot columns added are NUMERIC, 0 -> 1
        for colName in oneHotNameList:
            # Remove the pre one hot columns
            derivedCsvMetadata.remove(colName)
        for colName in (set(df.columns) - initialColNameSet):
            # Add the new one hot columns
            if colName.endswith('_-1'):
                df = df.drop(colName, axis = 1)
                continue
            derivedCsvMetadata.replace(CsvColumnMetadata(colName,
                                                         CsvColumnType.NUMERIC,
                                                         ''))

    # ordinals are 1 offset, we want 0 -> 1 for boolean & 0
    # offset if there were any others, so subtract 1
    df.loc[:, ordinalNameList] -= 1

    # TODO FIXME - assumes ordinal_features are only for output
    # allDerivedInputCols = list(set(X.columns) - set(ordinal_features))
#     allDerivedCsvCols = df.columns

    numericCols = derivedCsvMetadata.getTypedColumnNameList(set([CsvColumnType.NUMERIC])) + ordinalNameList

    # convert to numerical
    df = df[numericCols].apply(pd.to_numeric)

    colAttribDf = calcStats(df, optNumericCols = numericCols)

    if diag:
        logging.info('Shape before trimming=' + str(df.shape))

    if optCustomStdevTrimFeaturesList is None:
        # Trim outliers, based on all input cols
        trim_features = list(set(df.columns.difference(set(derivedCsvMetadata.getOutputColumnNameList()))))
    else:
        # Only trim based on selected input cols
        trim_features = optCustomStdevTrimFeaturesList

    for feature_name in trim_features:
        mean_value = colAttribDf.loc[ feature_name, 'mean']
        std_value = colAttribDf.loc[ feature_name, 'std']
        df = df.loc[np.abs(df[feature_name] - mean_value) <= (trim_stdev * std_value)]
        if diag:
            logging.info(feature_name + '=' + str(df.shape))

    # save to csv
#     if diag and optOutputDir is not None:
#         filename = os.path.basename(inputCsvFilename)
#         df.to_csv(optOutputDir + filename + '_trimmed_not_normalised.csv', sep = ',')
    result = df.copy()
    if not skipNormalizing:
        for feature_name in numericCols:
            max_value = df[feature_name].max()
            min_value = df[feature_name].min()
            norm_range = max_value - min_value
            if norm_range == 0:
                norm_range = 1
            result[feature_name] = (df[feature_name] - min_value) / norm_range
        df = result
#     if diag and optOutputDir is not None:
#         df.to_csv(optOutputDir + filename + '_trimmed_normalised.csv', sep = ',')
    if diag:
       logging.info('after all processing=' + str(df.shape))
    return df, derivedCsvMetadata


def dfcol2array(df):
    '''
    Convert a single column dataframe to a single column nunmpy array,
    removing headers, index
    '''
    return df.iloc[:, 0].values


def dfToCsv(df, basefilepath):
    filepath = basefilepath + '.csv'
#     logging.info('dfToCsv: ' + filepath)
    df.to_csv(filepath , sep = ',')


#------------------------------------------------------------------------------8
# Resampling without replacement
#       input dataframe and sample size
#       returning dataframe
#-------------------------------------------------------------------------------
def resample_without_replacement_df(df, requestedRowCount, acceptFewerRows = False):
    np_arr = df.values
    cols = df.columns.tolist()
    if(not acceptFewerRows and np_arr.size < requestedRowCount):
        raise Exception("requestedRowCount({}) > dfRowCount({})".format(requestedRowCount, np_arr.size))

    actualRowCount = min(np_arr.size, requestedRowCount)

    indices = np.random.permutation(np_arr.shape[0])
    sample_idx, unused_idx = indices[:actualRowCount + 1], indices[actualRowCount + 1:]
    data_sample, data_unused = np_arr[sample_idx, :], np_arr[unused_idx, :]
    data_sample_df = pd.DataFrame(data_sample, columns = cols)
    return data_sample_df


# TODO consolidate with resample_without_replacement_df()
def resample_without_replacement(df, requestedRowCount, acceptFewerRows = False):
    dfRowCount = df.shape[0]
    if(not acceptFewerRows and dfRowCount < requestedRowCount):
        raise Exception("requestedRowCount({}) > dfRowCount({})".format(requestedRowCount, dfRowCount))

    actualRowCount = dfRowCount
    if requestedRowCount < 0:
        # -1 means just use df row count
        actualRowCount = dfRowCount
#    return df.iloc[np.random.randint(0, len(df), size = requestedRowCount)]

    indices = np.random.permutation(df.shape[0])
    sample_idx, unused_idx = indices[:actualRowCount + 1], indices[actualRowCount + 1:]
    data_sample_df, data_unused = df.iloc[sample_idx, :], df.iloc[unused_idx, :]
    return data_sample_df


#------------------------------------------------------------------------------8
# Resampling a dataframe into two dataframes based on split index
#       returning two dataframes
#-------------------------------------------------------------------------------
def random_split(df, split_index):
    if(df.size < split_index):
        raise Exception("split_index({}) > df.size({})".format(split_index, df.size))

    indices = np.random.permutation(df.shape[0])
    first_idx, second_idx = indices[:split_index + 1], indices[split_index + 1:]
    first_sample, second_sample = df[first_idx, :], df[second_idx, :]
    return first_sample, second_sample

#------------------------------------------------------------------------------8
# Split column wise, the last column is always Y, while the remainders are X
#       input dataframe and the number of columns
#       returning dataframe for all inputs and the output
#-------------------------------------------------------------------------------
# def xy_split_cols(df, number_of_cols):
#     Xvar = df[:, 0:number_of_cols - 1]
#     Yvar = df[:, number_of_cols - 1]
#     return Xvar, Yvar


def xy_split_cols_df(df, optXColumnCount = None):
    '''
    Split a df into 2 dfs. Xdf is all inputs (assumed to
    occupy all but the last column, unless optXColumnCount specified).
    Ydf is only the last column.
    '''
    xColumnCount = optXColumnCount
    if optXColumnCount is None:
        xColumnCount = df.shape[1] - 1
    Xdf = df.iloc[:, 0:xColumnCount]
    Ydf = df.iloc[:, xColumnCount:xColumnCount + 1]
    return Xdf, Ydf

