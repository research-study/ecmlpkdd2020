'''
Created on 26Aug.,2019
'''

import psutil
import os
import threading
import logging
import time
import sys
from _signal import SIGTERM, SIGKILL
import subprocess
from subprocess import CalledProcessError, STDOUT
from tensorflow.compiler.xla.xla_data_pb2 import LOWEST_PAD
import multiprocessing


class ProcessUtil(object):
    '''
    classdocs
    '''

    @staticmethod
    def getCpuCount():
        '''
        Get the number of CPU cores
        '''
#         p = psutil.Process()
        # Set affinity of this process to run on cpu #0 only
        # i.e. single threaded
#         cpuList = p.cpu_affinity()
#         return len(cpuList)
        return multiprocessing.cpu_count()

    @staticmethod
    def setAffinity(coreIndex):
        '''
        Set the affinity of this process to a single CPU core, index coreIndex
        '''
        p = psutil.Process()
        # Set affinity of this process to run on cpu #0 only
        # i.e. single threaded
        cpuList = p.cpu_affinity()
        idx = min(coreIndex, len(cpuList) - 1)
        p.cpu_affinity(cpus = cpuList[idx:idx + 1])
        
    @classmethod
    def setAffinityToLowestCpuUsageCore(cls,
                                         diag = False):
        i = cls.getCoreIndexWithLowestCpuUsage(diag = diag)
        if diag:
            c = ProcessUtil.getCpuCount()
            print(f'Setting affinity to CPU code index {i} from {c} cores')
        ProcessUtil.setAffinity(i)

    @classmethod
    def getCoreIndexWithLowestCpuUsage(cls,
                                       diag = False):
        '''
         Wait for 1 second to see which core has the lowest cpu
         usage and return its zero based index
        '''
        cpuUsageTuple = psutil.cpu_percent(interval=2, percpu=True)
        if diag:
            print(f'cpuUsageTuple={cpuUsageTuple}')
        return cpuUsageTuple.index(min(cpuUsageTuple))
        
    @classmethod
    def getMyPid(cls):
        return os.getpid()

    @classmethod
    def killSelf(cls,
                 sig = SIGKILL):
        os.kill(cls.getMyPid(), sig)

    @classmethod
    def getParentPid(cls):
        return os.getppid()
    
    class ChildProcessExitMonitor(object):
        '''
        Thread to monitor when parent has exited, and exit in response
        '''
        def __init__(self, 
                     pollTimeSec : int):
            threading.Thread.__init__(self)
            self.pollTimeSec = pollTimeSec
            thread = threading.Thread(target = self.monitor)
            thread.start()

        def monitor(self):
            myPid : int = ProcessUtil.getMyPid()
            myInitialParentPid : int = ProcessUtil.getParentPid()
            logging.info(f'Monitor this pid={myPid} for change in parent pid, initially={myInitialParentPid}')
            while True:
                time.sleep(self.pollTimeSec)
                myParentPid : int = ProcessUtil.getParentPid()
                if myParentPid != myInitialParentPid:
                    logging.info(f'EXITING, because parent has stopped, parent pid is now {myParentPid}')
                    ProcessUtil.killSelf(sig = SIGKILL)


    @staticmethod
    def getstatusoutput(cmd, **kwargs):
        """
        
        Same as subprocess.getstatusoutput)(, but allows kwargs to be passed (ex. for passing Env)
        
        Return (exitcode, output) of executing cmd in a shell.
    
        Execute the string 'cmd' in a shell with 'check_output' and
        return a 2-tuple (status, output). The locale encoding is used
        to decode the output and process newlines.
    
        A trailing newline is stripped from the output.
        The exit status for the command can be interpreted
        according to the rules for the function 'wait'. Example:
    
        >>> import subprocess
        >>> subprocess.getstatusoutput('ls /bin/ls')
        (0, '/bin/ls')
        >>> subprocess.getstatusoutput('cat /bin/junk')
        (1, 'cat: /bin/junk: No such file or directory')
        >>> subprocess.getstatusoutput('/bin/junk')
        (127, 'sh: /bin/junk: not found')
        >>> subprocess.getstatusoutput('/bin/kill $$')
        (-15, '')
        """
        try:
            data = subprocess.check_output(cmd, shell=True, universal_newlines=True, stderr=STDOUT, **kwargs)
            exitcode = 0
        except CalledProcessError as ex:
            data = ex.output
            exitcode = ex.returncode
        if data[-1:] == '\n':
            data = data[:-1]
        return exitcode, data
