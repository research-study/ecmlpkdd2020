'''
Created on 1Oct.,2019
'''
import shap
import numpy as np
import pandas as pd
# from sklearn.feature_selection.tests.test_base import feature_names
# from skimage.feature.tests.test_util import plt
import matplotlib.pyplot as plt

from ShapSummaryPlot import summary_plot2
from PandasHelper import dfToCsv


class ExplainerUtil(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''

    @staticmethod
    def calcKerasFeatureImportance(kerasModel,
                                   xTrainDf,
                                   xTestDf,
                                   proportionOfTrainRowsToTrainWith = 1.0,
                                   proportionOfTestRowsToAssessWith = 1.0,
                                   plotSummaryFeatureImportance = False,
                                   optSummaryFeatureImportanceFile = None,
                                   optSummaryFeatureImportancePlotFile = None):
        '''
        Calculate the feature importances for each output class

        kerasModel is a Keras Model
        xTrainDf is a training Dataframe containing rows of training data, for all input model columns.
        It is used to train the SHAP model
        xTestDf is a test Dataframe containing rows of test data, for all input model columns
        It is used to evaluate the importance of the test data
        proportionOfTrainRowsToTrainWith and proportionOfTestRowsToAssessWith can be reduced (at the
        cost of lower accuracy) to reduce processing time
        plotSummaryFeatureImportance can be set to True to generate a plot, it must be True
        for the plot to be stored in optSummaryFeatureImportancePlotFile. If optSummaryFeatureImportancePlotFile
        is None and plotSummaryFeatureImportance is True, the plot will only be shown on the screen
        '''
        # select a set of background examples to take an expectation over
        explainerXTrain = xTrainDf.sample(frac = proportionOfTrainRowsToTrainWith, replace = False)

        # explain predictions of the model
        e = shap.DeepExplainer(kerasModel, explainerXTrain)

        # ...or pass tensors directly
        # e = shap.DeepExplainer((model.layers[0].input, model.layers[-1].output), background)

        shap_values = e.shap_values(xTestDf.values[0:int(proportionOfTestRowsToAssessWith * xTestDf.shape[0])])

        # Calc mean relative importance for each feature
        global_shap_values = np.abs(shap_values).mean(0)
        class_inds = np.argsort([-np.abs(shap_values[i]).mean() for i in range(len(shap_values))])
        feature_order = np.argsort(np.sum(np.mean(np.abs(shap_values), axis = 0), axis = 0))
#         classIndexToFeatureNameToImportanceDict = {}
        classImportanceDf = pd.DataFrame(columns = xTestDf.columns)
        for i, ind in enumerate(class_inds):
            global_shap_values = np.abs(shap_values[ind]).mean(0)
            importance = global_shap_values[feature_order]
            featureNameToImportanceDict = {}
            for j in range(0, len(importance)):
                featureName = xTestDf.columns[feature_order[j]]
                featureNameToImportanceDict[featureName] = importance[j]
#             classIndexToFeatureNameToImportanceDict[i] = featureNameToImportanceDict
            classImportanceDf.loc[len(classImportanceDf.index)] = featureNameToImportanceDict

                # plot the feature attributions
        if plotSummaryFeatureImportance:
            # Show on screen if not being saved to a file
            show = optSummaryFeatureImportancePlotFile is None
            summary_plot2(shap_values,
                              xTestDf.values[0:int(proportionOfTestRowsToAssessWith * xTestDf.shape[0])],
                              plot_type = "bar",
                              show = show,
                              feature_names = xTestDf.columns)
            if optSummaryFeatureImportancePlotFile is not None:
                plt.savefig(optSummaryFeatureImportancePlotFile, bbox_inches = "tight")
                plt.close('all')
        if optSummaryFeatureImportanceFile is not None:
            dfToCsv(classImportanceDf, optSummaryFeatureImportanceFile)
        return classImportanceDf
#         shap.image_plot(shap_values, -xTestDf.values[0:int(proportionOfTestRowsToAssessWith * xTestDf.shape[0])])

    @staticmethod
    def calcGbmFeatureImportance(gbmModel,
                                   xTrainDf,
                                   xTestDf,
                                   optExplainerSpec = None,
                                   proportionOfTrainRowsToTrainWith = 1.0,
                                   proportionOfTestRowsToAssessWith = 1.0,
                                   plotSummaryFeatureImportance = False,
                                   optSummaryFeatureImportanceFile = None,
                                   optSummaryFeatureImportancePlotFile = None):
        '''
        Calculate the feature importances for each output class

        gbmModel is a GBM (XGBoost, CatBoost, LightGBM) Model
        optExplainerModel, if provided, will be used instead of training a model
        xTrainDf is a training Dataframe containing rows of training data, for all input model columns.
        It is used to train the SHAP model
        xTestDf is a test Dataframe containing rows of test data, for all input model columns
        It is used to evaluate the importance of the test data
        proportionOfTrainRowsToTrainWith and proportionOfTestRowsToAssessWith can be reduced (at the
        cost of lower accuracy) to reduce processing time
        plotSummaryFeatureImportance can be set to True to generate a plot, it must be True
        for the plot to be stored in optSummaryFeatureImportancePlotFile. If optSummaryFeatureImportancePlotFile
        is None and plotSummaryFeatureImportance is True, the plot will only be shown on the screen
        '''
        if optExplainerSpec is None:
            # select a set of background examples to take an expectation over
            explainerXTrain = xTrainDf.sample(frac = proportionOfTrainRowsToTrainWith, replace = False)

            # explain predictions of the model
    #         e = shap.TreeExplainer(gbmModel, explainerXTrain)  # , feature_dependence = "independent")
            e = shap.TreeExplainer(gbmModel, explainerXTrain, feature_dependence = "independent")
        else:
            e = optExplainerSpec.getExplainerModel()
        # ...or pass tensors directly
        # e = shap.DeepExplainer((model.layers[0].input, model.layers[-1].output), background)

# Need to ensure ALL leaves are visited in EVERY fold, else we get this error"
# The background dataset you provided does not cover all the leaves in the model,
# so TreeExplainer cannot run with the feature_dependence="tree_path_dependent" option! 
# Try providing a larger background dataset, or using feature_dependence="independent".

        shap_values = e.shap_values(xTestDf.values[0:int(proportionOfTestRowsToAssessWith * xTestDf.shape[0])])
        # TODO, determine why this is different to the DeepExplainer case
        shap_values = [shap_values]

        # Calc mean relative importance for each feature
        global_shap_values = np.abs(shap_values).mean(0)
        class_inds = np.argsort([-np.abs(shap_values[i]).mean() for i in range(len(shap_values))])
        feature_order = np.argsort(np.sum(np.mean(np.abs(shap_values), axis = 0), axis = 0))
#         classIndexToFeatureNameToImportanceDict = {}
        classImportanceDf = pd.DataFrame(columns = xTestDf.columns)
        for i, ind in enumerate(class_inds):
            global_shap_values = np.abs(shap_values[ind]).mean(0)
            importance = global_shap_values[feature_order]
            featureNameToImportanceDict = {}
            for j in range(0, len(importance)):
                featureName = xTestDf.columns[feature_order[j]]
                featureNameToImportanceDict[featureName] = importance[j]
#             classIndexToFeatureNameToImportanceDict[i] = featureNameToImportanceDict
            classImportanceDf.loc[len(classImportanceDf.index)] = featureNameToImportanceDict

                # plot the feature attributions
        if plotSummaryFeatureImportance:
            # Show on screen if not being saved to a file
            show = optSummaryFeatureImportancePlotFile is None
            summary_plot2(shap_values,
                              xTestDf.values[0:int(proportionOfTestRowsToAssessWith * xTestDf.shape[0])],
                              plot_type = "bar",
                              show = show,
                              feature_names = xTestDf.columns)
            if optSummaryFeatureImportancePlotFile is not None:
                plt.savefig(optSummaryFeatureImportancePlotFile, bbox_inches = "tight")
                plt.close('all')
        if optSummaryFeatureImportanceFile is not None:
            dfToCsv(classImportanceDf, optSummaryFeatureImportanceFile)
        return classImportanceDf
#         shap.image_plot(shap_values, -xTestDf.values[0:int(proportionOfTestRowsToAssessWith * xTestDf.shape[0])])

