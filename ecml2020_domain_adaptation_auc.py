#
# to compare model trained using source only, target only, source then target, adapted source then target
#
import pandas as pd
import numpy as np
from scipy.spatial import distance
from keras.models import Model, load_model
from keras.layers import Dense, Input
from sklearn.utils import class_weight
from sklearn.model_selection import StratifiedKFold
from numpy.random import seed
import tensorflow as tf
import random as rn
import os
from copy import deepcopy
import pandasql as psql
from ExplainerUtil import ExplainerUtil
from ProcessUtil import ProcessUtil

# See https://stackoverflow.com/questions/3285193/how-to-change-backends-in-matplotlib-python
import matplotlib.pyplot as plt
plt.switch_backend('Agg')
import matplotlib
print (f'Using matplotlib {matplotlib.pyplot.get_backend()}')

from typing import Optional, List
import sys
import logging
import getopt
import traceback


similarity_threshold = 0.1
number_of_col = 13
number_of_input = 12
number_of_output = 1
number_of_layers = 3
number_of_nodes = 32
number_of_all_layers = number_of_layers + 2
retraining_last_layer = True


# to convert dataframe to csv
def dfToCsv(df, basefilepath):
    filepath = basefilepath + '.csv'
    df.to_csv(filepath , sep = ',')

# append a row to dataframe
def appendRowToDf(df, r):
    df.loc[len(df.index)] = r

# resample from dataframe without replacement
def resample_without_replacement_df(df, requestedRowCount, acceptFewerRows = False):
    np_arr = df.values
    cols = df.columns.tolist()
    if(not acceptFewerRows and np_arr.size < requestedRowCount):
        raise Exception("requestedRowCount({}) > dfRowCount({})".format(requestedRowCount, np_arr.size))

    actualRowCount = min(np_arr.size, requestedRowCount)

    indices = np.random.permutation(np_arr.shape[0])
    sample_idx, unused_idx = indices[:actualRowCount + 1], indices[actualRowCount + 1:]
    data_sample, data_unused = np_arr[sample_idx, :], np_arr[unused_idx, :]
    data_sample_df = pd.DataFrame(data_sample, columns = cols)
    return data_sample_df

# locked random seed
def setRandomSeed(randomSeed):
    os.environ["PYTHONHASHSEED"] = "0"
    # Multiple libraries
    np.random.seed(randomSeed)
    # Tensorflow
    tf.set_random_seed(randomSeed)
    # For python hashes
    rn.seed(randomSeed)

# split input (x) and output (y)
def xy_split(one_dataframe,number_of_col):
    x = one_dataframe.iloc[:,0:number_of_col-1]
    y = one_dataframe.iloc[:,number_of_col-1]
    return x,y

# def normalise(one_dataframe):
#     for feature in one_dataframe.columns:
#         max_value = one_dataframe[feature].max()
#         min_value = one_dataframe[feature].min()
#         range_value = max_value-min_value
#         if range_value == 0:
#             range_value = 1
#         one_dataframe[feature] = (one_dataframe[feature] - min_value) / range_value
#     return one_dataframe

def xnormalise(one_dataframe):
    list_of_columns = one_dataframe.columns
    num_columns = list_of_columns.size
    normalise_params_df = pd.DataFrame(index=np.arange(0, num_columns-1),columns=('feature', 'min_value', 'range_value')) #num_columns-1 to remove outcome
    feature_index = 0
    for feature in list_of_columns:
        if feature != 'Outcome':
            max_value = one_dataframe[feature].max()
            min_value = one_dataframe[feature].min()
            range_value = max_value-min_value
            if range_value == 0:
                range_value = 1
            one_dataframe[feature] = (one_dataframe[feature] - min_value) / range_value
            normalise_params_df.loc[feature_index] = feature, min_value, range_value
            feature_index = feature_index + 1
    return one_dataframe, normalise_params_df

def xdenormalise(one_dataframe, normalise_params_df):
    list_of_columns = one_dataframe.columns
    num_columns = list_of_columns.size
    for index, row in normalise_params_df.iterrows():
        feature = row['feature']
        min_value = row['min_value']
        range_value = row['range_value']
        if feature != 'Outcome':
            one_dataframe[feature] = (one_dataframe[feature]  * range_value) + min_value
    return one_dataframe

def calc_auc(fscore, foutcome):
    n = len(fscore)
    n_pos = sum(foutcome)
    #print("n_pos="+str(n_pos))
    df = pd.DataFrame({'score': fscore, 'outcome': foutcome})
    df = df.sort_values('score', ascending = False)
    df['seq'] = list(range(1, n + 1))
    df['cum_outcome'] = df['outcome'].cumsum()
    df['above'] = df['seq'] - df['cum_outcome']
    df['area'] = df['above'] * df['outcome']
    auc = 1 - (df['area'].sum() / (n_pos * (n - n_pos)))
    return auc

def calc_gini(fscore, foutcome):
    gini = calc_auc(fscore, foutcome) * 2 - 1
    return gini

def weight_balance(Yvar):
    return class_weight.compute_class_weight('balanced', np.unique(Yvar), Yvar)

def make_last_layer_retrainable(one_model):
    cnt = 0
    for layer in one_model.layers:
        if (cnt == 4):  # from == 4
            layer.trainable = True
        else:
            layer.trainable = False
        cnt = cnt + 1
    one_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return one_model


def initial_training_source(outputDir, model_name, list_of_seeds, n_fold, datapath, sample_size):
    number_of_models = len(list_of_seeds) * n_fold
    performance = pd.DataFrame(index=np.arange(0, number_of_models), columns=('repeat', 'iteration', 'training', 'test', 'auc'))

    features = [
        'term_36m'
        , 'term_60m'
        , 'grade_n'
        , 'sub_grade_n'

        , 'revol_util_n'
        , 'int_rate_n'
        , 'installment_n'
        , 'emp_length_n'

        , 'dti_n'
        , 'annual_inc_n'
        , 'loan_amnt_n'
        , 'cover'
        , 'Outcome'
    ]

    featureImportanceDf = pd.DataFrame(columns=features)

    repeat = 0
    for a_random_seed in list_of_seeds:
        np.random.seed(a_random_seed)

        temp_dataset = pd.read_csv(datapath)
        temp_dataset = temp_dataset[features]
        sample_dataset = resample_without_replacement_df(temp_dataset,sample_size,True)
        one_dataset, normalise_param = xnormalise(sample_dataset)

        Xdata, Ydata = xy_split(one_dataset, 13)
        XYdata = one_dataset

        skf = StratifiedKFold(n_splits=n_fold, random_state=None, shuffle=False)
        #print("Number of splits = " + str(skf.get_n_splits(Xfull_target, Yfull_target, XYfull_target)))
        class_weights = weight_balance(Ydata)

        iteration = 0
        for train_index, test_index in skf.split(Xdata, Ydata, XYdata):

            # 2-define the empty list to store the layers
            dl_layer = [None] * (number_of_all_layers)
            # 3-define the number of nodes as an input for the first list (input function in keras)
            dl_layer[0] = Input(shape=(number_of_input,), name='NNinput')
            # 4- this comment connects the layers of NN to each other (if i is 1 means layer 1 connected to layer o and if i is 2 means layer 2 connected to 1 and so on.
            for i in range(1, number_of_layers + 1):
                dl_layer[i] = Dense(number_of_nodes, activation='relu', name='dense_function' + str(i))(dl_layer[i - 1])
            output = Dense(number_of_output, activation='sigmoid', name='NNoutput')(dl_layer[number_of_layers])
            one_model = Model(inputs=[dl_layer[0]], outputs=output)
            one_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            process = "model creation...model compile..."

            # to split x and y for training
            XYtrain, XYtest = XYdata.iloc[train_index, :], XYdata.iloc[test_index, :]
            Xtrain, Xtest = Xdata.iloc[train_index, :], Xdata.iloc[test_index, :]
            Ytrain, Ytest = Ydata.iloc[train_index], Ydata.iloc[test_index]

            one_model.fit(Xtrain, Ytrain, epochs=50, batch_size=25, class_weight=class_weights, verbose=0)

            foldClassImportanceDf = ExplainerUtil.calcKerasFeatureImportance(one_model,
                                                                             Xtrain,
                                                                             Xtest,
                                                                             proportionOfTrainRowsToTrainWith=1.0,
                                                                             proportionOfTestRowsToAssessWith=1.0,
                                                                             plotSummaryFeatureImportance=True,
                                                                             optSummaryFeatureImportancePlotFile=f"{outputDir}/source_featureImportanceSummary-repeat-{repeat}-iteration-{iteration}.png")
            appendRowToDf(featureImportanceDf, foldClassImportanceDf.loc[0])

            Ypred = (one_model.predict(Xtest)).flatten()
            model_index = (repeat * n_fold) + iteration
            one_model.save(outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(model_index) + ".h5")
            performance.loc[model_index] = repeat, iteration, Ytrain.size, Ytest.size, calc_auc(Ypred, Ytest)
            print(process+" repeat="+str(repeat)+"  Iteration="+str(iteration)+"   Train:", train_index.size, "   Test:", test_index.size, "   AUC:", calc_auc(Ypred, Ytest))
            iteration = iteration + 1
        repeat = repeat + 1


    dfToCsv(featureImportanceDf, f'{outputDir}/source_featureImportanceDf')
    # Clear plot axis & figures
    plt.cla()
    plt.clf()
    plt.close('all')
    meanFeatureImportanceDf = featureImportanceDf.mean().sort_values()
    dfToCsv(meanFeatureImportanceDf, f'{outputDir}/source_meanFeatureImportanceDf')
    meanFeatureImportanceDf.plot(kind='barh')
    plt.savefig(f'{outputDir}/source_meanFeatureImportanceDf.png', bbox_inches='tight')
    plt.cla()
    plt.clf()
    plt.close('all')

    std_auc = performance.loc[:, "auc"].std()
    avg_auc = performance.loc[:, "auc"].mean()
    # first = True
    error = [None] * (number_of_models)
    min_error = 999
    min_model_index = -1
    for i in range(0, number_of_models):
        error[i] = abs(performance.loc[i, "auc"] - avg_auc)
        if error[i] < min_error:
            min_error = error[i]
            min_model_index = i
    model_filename = outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(min_model_index) + ".h5"
    print("Loading the average model=" + model_filename)
    print(model_name+" average AUC="+str(avg_auc)+"  stdev="+str(std_auc))
    average_model = load_model(model_filename)
    return average_model, model_filename, avg_auc



def initial_training_target(outputDir, model_name, list_of_seeds, n_fold, datapath, sample_size):
    number_of_models = len(list_of_seeds) * n_fold
    performance = pd.DataFrame(index=np.arange(0, number_of_models), columns=('repeat', 'iteration', 'training', 'test', 'auc'))

    features = [
        'term_36m'
        , 'term_60m'
        , 'grade_n'
        , 'sub_grade_n'

        , 'revol_util_n'
        , 'int_rate_n'
        , 'installment_n'
        , 'emp_length_n'

        , 'dti_n'
        , 'annual_inc_n'
        , 'loan_amnt_n'
        , 'cover'
        , 'Outcome'
    ]

    featureImportanceDf = pd.DataFrame(columns=features)

    repeat = 0
    for a_random_seed in list_of_seeds:
        np.random.seed(a_random_seed)

        temp_dataset = pd.read_csv(datapath)
        temp_dataset = temp_dataset[features]
        sample_dataset = resample_without_replacement_df(temp_dataset,sample_size,True)
        one_dataset, normalise_param = xnormalise(sample_dataset)

        Xdata, Ydata = xy_split(one_dataset, 13)
        XYdata = one_dataset

        skf = StratifiedKFold(n_splits=n_fold, random_state=None, shuffle=False)
        #print("Number of splits = " + str(skf.get_n_splits(Xfull_target, Yfull_target, XYfull_target)))
        class_weights = weight_balance(Ydata)

        iteration = 0
        for train_index, test_index in skf.split(Xdata, Ydata, XYdata):

            # 2-define the empty list to store the layers
            dl_layer = [None] * (number_of_all_layers)
            # 3-define the number of nodes as an input for the first list (input function in keras)
            dl_layer[0] = Input(shape=(number_of_input,), name='NNinput')
            # 4- this comment connects the layers of NN to each other (if i is 1 means layer 1 connected to layer o and if i is 2 means layer 2 connected to 1 and so on.
            for i in range(1, number_of_layers + 1):
                dl_layer[i] = Dense(number_of_nodes, activation='relu', name='dense_function' + str(i))(dl_layer[i - 1])
            output = Dense(number_of_output, activation='sigmoid', name='NNoutput')(dl_layer[number_of_layers])
            one_model = Model(inputs=[dl_layer[0]], outputs=output)
            one_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            process = "model creation...model compile..."

            # to split x and y for training
            XYtrain, XYtest = XYdata.iloc[train_index, :], XYdata.iloc[test_index, :]
            Xtrain, Xtest = Xdata.iloc[train_index, :], Xdata.iloc[test_index, :]
            Ytrain, Ytest = Ydata.iloc[train_index], Ydata.iloc[test_index]

            one_model.fit(Xtrain, Ytrain, epochs=50, batch_size=25, class_weight=class_weights, verbose=0)

            foldClassImportanceDf = ExplainerUtil.calcKerasFeatureImportance(one_model,
                                                                             Xtrain,
                                                                             Xtest,
                                                                             proportionOfTrainRowsToTrainWith=1.0,
                                                                             proportionOfTestRowsToAssessWith=1.0,
                                                                             plotSummaryFeatureImportance=True,
                                                                             optSummaryFeatureImportancePlotFile=f"{outputDir}/target_featureImportanceSummary-repeat-{repeat}-iteration-{iteration}.png")
            appendRowToDf(featureImportanceDf, foldClassImportanceDf.loc[0])

            Ypred = (one_model.predict(Xtest)).flatten()
            model_index = (repeat * n_fold) + iteration
            one_model.save(outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(model_index) + ".h5")
            performance.loc[model_index] = repeat, iteration, Ytrain.size, Ytest.size, calc_auc(Ypred, Ytest)
            print(process+" repeat="+str(repeat)+"  Iteration="+str(iteration)+"   Train:", train_index.size, "   Test:", test_index.size, "   AUC:", calc_auc(Ypred, Ytest))
            iteration = iteration + 1
        repeat = repeat + 1


    dfToCsv(featureImportanceDf, f'{outputDir}/target_featureImportanceDf')
    # Clear plot axis & figures
    plt.cla()
    plt.clf()
    plt.close('all')
    meanFeatureImportanceDf = featureImportanceDf.mean().sort_values()
    dfToCsv(meanFeatureImportanceDf, f'{outputDir}/target_meanFeatureImportanceDf')
    meanFeatureImportanceDf.plot(kind='barh')
    plt.savefig(f'{outputDir}/target_meanFeatureImportanceDf.png', bbox_inches='tight')
    plt.cla()
    plt.clf()
    plt.close('all')

    std_auc = performance.loc[:, "auc"].std()
    avg_auc = performance.loc[:, "auc"].mean()
    # first = True
    error = [None] * (number_of_models)
    min_error = 999
    min_model_index = -1
    for i in range(0, number_of_models):
        error[i] = abs(performance.loc[i, "auc"] - avg_auc)
        if error[i] < min_error:
            min_error = error[i]
            min_model_index = i
    model_filename = outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(min_model_index) + ".h5"
    print("Loading the average model=" + model_filename)
    print(model_name+" average AUC="+str(avg_auc)+"  stdev="+str(std_auc))
    average_model = load_model(model_filename)
    return average_model, model_filename, avg_auc

def adaptation(df_source, df_target, varname, num_quantile, recalculate_cover):
    varname_q = 'quantile'

    tgt_min = 'tgt_min'
    tgt_max = 'tgt_max'
    tgt_cnt = 'tgt_cnt'
    tgt_df = 'df_target'
    tgt_rng = 'tgt_rng'

    src_min = 'src_min'
    src_max = 'src_max'
    src_cnt = 'src_cnt'
    src_df = 'df_source'
    src_rng = 'src_rng'

    scale = 'scale'

    #print('var name=' + varname)
    if (varname=='cover' and recalculate_cover==True):
        print('recalculate cover, annual income and loan amount need to be adapted first')
        df_source_m = deepcopy(df_source)
        df_source_m['cover'] = df_source_m['annual_inc_n'] / df_source_m['loan_amnt_n']
        source_sum = None
        target_sum = None
        source_sum_x = None
    else:
        df_target[varname_q] = pd.qcut(df_target[varname], q=num_quantile, labels=False, duplicates='drop')
        target_sum = psql.sqldf(
            'select ' + varname_q + ', min(' + varname + ') as ' + tgt_min + ', max(' + varname + ') as ' + tgt_max + ', count(1) as ' + tgt_cnt + ' from ' + tgt_df + ' group by 1')

        # summary of source outcome
        df_source[varname_q] = pd.qcut(df_source[varname], q=num_quantile, labels=False, duplicates='drop')
        source_sum = psql.sqldf(
            'select ' + varname_q + ', min(' + varname + ') as ' + src_min + ', max(' + varname + ') as ' + src_max + ', count(1) as ' + src_cnt + ' from ' + src_df + ' group by 1')

        # joining summary of target and source outcome
        tgt_src = pd.merge(target_sum, source_sum, on=[varname_q])
        tgt_src[tgt_rng] = tgt_src[tgt_max] - tgt_src[tgt_min]
        tgt_src[src_rng] = tgt_src[src_max] - tgt_src[src_min]
        tgt_src[scale] = tgt_src[tgt_rng] / tgt_src[src_rng]
        tgt_src = tgt_src.drop([tgt_cnt, src_cnt, tgt_max, src_max], axis=1)

        # joining the summary with the source outcome
        varadpt = varname + '_adpt'
        df_source_m = pd.merge(df_source, tgt_src, how='left', on=[varname_q])
        df_source_m[varadpt] = ((df_source_m[varname] - df_source_m[src_min]) * df_source_m[scale]) + df_source_m[tgt_min]
        df_source_m = df_source_m.drop([tgt_min, src_min, tgt_rng, src_rng, scale, varname_q, varname], axis=1)
        df_source_m.rename(columns={varadpt: varname}, inplace=True)
        col_list = ['term_36m', 'term_60m', 'grade_n', 'sub_grade_n', 'revol_util_n', 'int_rate_n', 'installment_n',
                    'tot_hi_cred_lim_n', 'emp_length_n', 'dti_n', 'avg_cur_bal_n', 'all_util_n', 'acc_open_past_24mths_n',
                    'annual_inc_n', 'loan_amnt_n', 'cover', 'Outcome']
        df_source_m = df_source_m[col_list]
        df_source_x = deepcopy(df_source_m)
        df_source_x[varname_q] = pd.qcut(df_source_x[varname], q=num_quantile, labels=False, duplicates='drop')
        source_sum_x = psql.sqldf(
            'select ' + varname_q + ', min(' + varname + ') as ' + src_min + ', max(' + varname + ') as ' + src_max + ', count(1) as ' + src_cnt + ' from df_source_x group by 1')

    return df_source_m


def adaptation_equal_width(df_source, df_target, varname):
    print('equal_width adaptation var=', varname)
    target_sum = psql.sqldf('select ' + varname + ', count(1) as ' + varname + '_cnt from df_target group by 1')
    n_row_target = len(df_target.index)
    target_sum['n_row'] = n_row_target
    target_sum['percent_row'] = target_sum[varname + '_cnt'] / target_sum['n_row']

    df_source_sorted = deepcopy(df_source).sort_values(by=[varname], ascending=True)
    slice_varname = df_source_sorted[varname]
    n_row_source = len(df_source_sorted.index)
    target_sum['n_row_source'] = n_row_source
    target_sum['source_count'] = (target_sum['percent_row'] * target_sum['n_row_source']).round(0).astype(int)
    sum_source_count = target_sum['source_count'].sum()

    #print('sum source count=', sum_source_count)
    diff = n_row_source - sum_source_count
    #target_sum['source_count_adj'] = target_sum.apply(lambda x: myfunc(x.emp_length_n, x.source_count, diff), axis=1)

    # this part to fix rounding difference
    first = True
    for index, row in target_sum.iterrows():
        if first == True:
            target_sum['source_count_adj'] = target_sum['source_count'] + diff
            first = False
        else:
            target_sum['source_count_adj'] = target_sum['source_count']

    source_series = pd.Series()
    for index, row in target_sum.iterrows():
        varname_val = row[varname]
        source_count_adj = row['source_count_adj'].astype(int)
        tmp_series = pd.Series(np.tile([varname_val], source_count_adj))
        source_series = source_series.append(tmp_series)
    #print(target_sum)
    #print('len source series=', len(source_series))
    #print('len source sample sorted=', len(df_source_sorted.index))
    df_source_sorted.reset_index(drop=True, inplace=True)
    source_series_df = pd.DataFrame(source_series, columns=[varname])
    source_series_df.reset_index(drop=True, inplace=True)
    #print('len source series df=', len(source_series_df))
    #print('len source sample sorted df=', len(df_source_sorted.index))
    df_source_sorted = df_source_sorted.drop([varname], axis=1)
    #print(df_source_sorted.head())
    df_source_m = pd.concat([df_source_sorted, source_series_df], axis=1)
    col_list = ['term_36m', 'term_60m', 'grade_n', 'sub_grade_n', 'revol_util_n', 'int_rate_n', 'installment_n',
                'tot_hi_cred_lim_n', 'emp_length_n', 'dti_n', 'avg_cur_bal_n', 'all_util_n', 'acc_open_past_24mths_n',
                'annual_inc_n', 'loan_amnt_n', 'cover', 'Outcome']
    df_source_m = df_source_m[col_list]
    #print(df_source_m.head())
    return df_source_m

def adaptation_only(df_source, df_target, varname, num_quantile):
    print('adapting only: ', varname)
    #varname = 'dti_n'
    varname_q = 'quantile'

    tgt_min = 'tgt_min'
    tgt_max = 'tgt_max'
    tgt_cnt = 'tgt_cnt'
    tgt_df = 'df_target'
    tgt_rng = 'tgt_rng'

    src_min = 'src_min'
    src_max = 'src_max'
    src_cnt = 'src_cnt'
    src_df = 'df_source'
    src_rng = 'src_rng'

    scale = 'scale'

    #print('var name=' + varname)

    df_target[varname_q] = pd.qcut(df_target[varname], q=num_quantile, labels=False, duplicates='drop')
    target_sum = psql.sqldf(
        'select ' + varname_q + ', min(' + varname + ') as ' + tgt_min + ', max(' + varname + ') as ' + tgt_max + ', count(1) as ' + tgt_cnt + ' from ' + tgt_df + ' group by 1')

    # summary of source outcome
    df_source[varname_q] = pd.qcut(df_source[varname], q=num_quantile, labels=False, duplicates='drop')
    source_sum = psql.sqldf(
        'select ' + varname_q + ', min(' + varname + ') as ' + src_min + ', max(' + varname + ') as ' + src_max + ', count(1) as ' + src_cnt + ' from ' + src_df + ' group by 1')

    #print('target summary=', target_sum)
    #print('source summary=',source_sum)

    # joining summary of target and source outcome
    tgt_src = pd.merge(target_sum, source_sum, on=[varname_q])
    tgt_src[tgt_rng] = tgt_src[tgt_max] - tgt_src[tgt_min]
    tgt_src[src_rng] = tgt_src[src_max] - tgt_src[src_min]
    tgt_src[scale] = tgt_src[tgt_rng] / tgt_src[src_rng]
    tgt_src = tgt_src.drop([tgt_cnt, src_cnt, tgt_max, src_max], axis=1)

    # joining the summary with the source outcome
    varadpt = varname + '_adpt'
    df_source_m = pd.merge(df_source, tgt_src, how='left', on=[varname_q])
    df_source_m[varadpt] = ((df_source_m[varname] - df_source_m[src_min]) * df_source_m[scale]) + df_source_m[tgt_min]
    df_source_m = df_source_m.drop([tgt_min, src_min, tgt_rng, src_rng, scale, varname_q, varname], axis=1)
    df_source_m.rename(columns={varadpt: varname}, inplace=True)
    col_list = ['term_36m', 'term_60m', 'grade_n', 'sub_grade_n', 'revol_util_n', 'int_rate_n', 'installment_n',
                'emp_length_n', 'dti_n',
                'annual_inc_n', 'loan_amnt_n', 'cover', 'Outcome']
    df_source_m = df_source_m[col_list]
    df_source_x = deepcopy(df_source_m)
    df_source_x[varname_q] = pd.qcut(df_source_x[varname], q=num_quantile, labels=False, duplicates='drop')
    source_sum_x = psql.sqldf(
        'select ' + varname_q + ', min(' + varname + ') as ' + src_min + ', max(' + varname + ') as ' + src_max + ', count(1) as ' + src_cnt + ' from df_source_x group by 1')

    return df_source_m



def split_adaptation(df_source_tmp, df_target_tmp, varname, num_quantile):
    df_target0 = deepcopy(df_target_tmp[df_target_tmp['Outcome'] == 0])
    df_target1 = deepcopy(df_target_tmp[df_target_tmp['Outcome'] == 1])
    df_source0 = deepcopy(df_source_tmp[df_source_tmp['Outcome'] == 0])
    df_source1 = deepcopy(df_source_tmp[df_source_tmp['Outcome'] == 1])
    df_source_final0 = adaptation_only(df_source0, df_target0, varname, num_quantile)
    df_source_final1 = adaptation_only(df_source1, df_target1, varname, num_quantile)
    df_source_final01 = df_source_final0.append(df_source_final1)
    return df_source_final01

def checkSelectedVars(selectedVarList : List[str],
                      availableVarList : List[str],
                      ):
    intSet = set(selectedVarList).intersection(set(availableVarList))
    if len(intSet) != len(selectedVarList):
        raise Exception(f'{selectedVarList.difference(availableVarList)} not found in {availableVarList}')
def with_labels_source_training_guided_by_target_training(outputDir,
                                                          model_name, 
                                                          list_of_seeds, 
                                                          n_fold, 
                                                          source_filepath, 
                                                          source_size, 
                                                          target_filepath, 
                                                          target_size, 
                                                          adapt, 
                                                          num_quantile, 
                                                          list_of_vars):
    features = [
        'term_36m'
        , 'term_60m'
        , 'grade_n'
        , 'sub_grade_n'

        , 'revol_util_n'
        , 'int_rate_n'
        , 'installment_n'
        , 'emp_length_n'

        , 'dti_n'
        , 'annual_inc_n'
        , 'loan_amnt_n'
        , 'cover'
        , 'Outcome'
    ]
    checkSelectedVars(list_of_vars, features)

    featureImportanceDf = pd.DataFrame(columns=features)

    number_of_models = len(list_of_seeds) * n_fold
    performance = pd.DataFrame(index=np.arange(0, number_of_models), columns=('repeat', 'iteration', 'training', 'test', 'auc'))
    repeat = 0
    for a_random_seed in list_of_seeds:
        np.random.seed(a_random_seed)

        #threshold = 0.02
        #source_extract = euclidean_extract(source_df, target_df, threshold)
        #source_sample = resample_without_replacement_df(source_extract, source_size,True)
        #source_data = normalise(source_sample)

        source_temp = pd.read_csv(source_filepath)
        source_temp = source_temp[features]
        source_sample = resample_without_replacement_df(source_temp, source_size, True)
        source_dataset, source_normalise_param = xnormalise(source_sample)

        target_temp = pd.read_csv(target_filepath)
        target_temp = target_temp[features]
        target_sample = resample_without_replacement_df(target_temp, target_size, True)
        target_dataset, target_normalise_param = xnormalise(target_sample)

        Xdata, Ydata = xy_split(target_dataset, 13)
        XYdata = target_dataset

        skf = StratifiedKFold(n_splits=n_fold, random_state=None, shuffle=False)
        #print("Number of splits = " + str(skf.get_n_splits(Xfull_target, Yfull_target, XYfull_target)))
        class_weights = weight_balance(Ydata)

        iteration = 0
        for train_index, test_index in skf.split(Xdata, Ydata, XYdata):

            # 2-define the empty list to store the layers
            dl_layer = [None] * (number_of_all_layers)
            # 3-define the number of nodes as an input for the first list (input function in keras)
            dl_layer[0] = Input(shape=(number_of_input,), name='NNinput')
            # 4- this comment connects the layers of NN to each other (if i is 1 means layer 1 connected to layer o and if i is 2 means layer 2 connected to 1 and so on.
            for i in range(1, number_of_layers + 1):
                dl_layer[i] = Dense(number_of_nodes, activation='relu', name='dense_function' + str(i))(dl_layer[i - 1])
            output = Dense(number_of_output, activation='sigmoid', name='NNoutput')(dl_layer[number_of_layers])
            one_model = Model(inputs=[dl_layer[0]], outputs=output)
            one_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            process1 = "model creation...model compile..."

            # to split x and y for training
            XYtrain, XYtest = XYdata.iloc[train_index, :], XYdata.iloc[test_index, :]
            # use Ytrain here to extract source
            process3 = str(source_dataset.shape)

            if adapt:
                df_target_tmp = deepcopy(XYtrain)
                df_source_tmp = deepcopy(source_dataset)

                for vname in list_of_vars:
                    df_source_tmp = split_adaptation(df_source_tmp, df_target_tmp, vname, num_quantile)
                    #print('v adapted=', vname,df_source_tmp.head())
            if adapt==False:
                XYtrain_src = deepcopy(source_dataset)
            else:
                XYtrain_src = deepcopy(df_source_tmp)

            Xtrain_src, Ytrain_src = xy_split(XYtrain_src, 13)
            Xtest_src, Ytest_src = xy_split(XYtrain_src, 13)  # note: using training as test

            # initial training using source data
            one_model.fit(Xtrain_src, Ytrain_src, epochs=50, batch_size=25, class_weight=class_weights, verbose=0)

            Ypred_src = (one_model.predict(Xtest_src)).flatten()

            #source_model = load_model(initial_model_file)
            one_model = make_last_layer_retrainable(one_model)
            process2 = "model transfer...model last layer made trainable..."

            # to split x and y for training
            XYtrain, XYtest = XYdata.iloc[train_index, :], XYdata.iloc[test_index, :]
            Xtrain, Xtest = Xdata.iloc[train_index, :], Xdata.iloc[test_index, :]
            Ytrain, Ytest = Ydata.iloc[train_index], Ydata.iloc[test_index]

            one_model.fit(Xtrain, Ytrain, epochs=50, batch_size=25, class_weight=class_weights, verbose=0)
            # model_index = (repeat * n_fold) + iteration
            # one_model.save("./DEBUG2_" + model_name + "_" + str(n_fold) + "_" + str(model_index) + ".h5")
            # Xtrain.to_csv("./DEBUG2_Xtrain.csv")
            # Xtest.to_csv("./DEBUG2_Xtest.csv")
            foldClassImportanceDf = ExplainerUtil.calcKerasFeatureImportance(one_model,
                                                                             Xtrain,
                                                                             Xtest,
                                                                             proportionOfTrainRowsToTrainWith=1.0,
                                                                             proportionOfTestRowsToAssessWith=1.0,
                                                                             plotSummaryFeatureImportance=True,
                                                                             optSummaryFeatureImportancePlotFile=f"{outputDir}/transfer_featureImportanceSummary-repeat-{repeat}-iteration-{iteration}.png")

            appendRowToDf(featureImportanceDf, foldClassImportanceDf.loc[0])
            Ypred = (one_model.predict(Xtest)).flatten()

            model_index = (repeat * n_fold) + iteration
            one_model.save(outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(model_index) + ".h5")
            performance.loc[model_index] = repeat, iteration, Ytrain.size, Ytest.size, calc_auc(Ypred, Ytest)
            print(process1 + process2 + " source= " + process3 + " repeat="+str(repeat)+"  Iteration="+str(iteration)+"   Train:", train_index.size, "   Test:", test_index.size, "   AUC:", calc_auc(Ypred, Ytest))
            iteration = iteration + 1
        repeat = repeat + 1


    dfToCsv(featureImportanceDf, f'{outputDir}/transfer_featureImportanceDf')
    # Clear plot axis & figures
    plt.cla()
    plt.clf()
    plt.close('all')
    meanFeatureImportanceDf = featureImportanceDf.mean().sort_values()
    dfToCsv(meanFeatureImportanceDf, f'{outputDir}/transfer_meanFeatureImportanceDf')
    meanFeatureImportanceDf.plot(kind='barh')
    plt.savefig(f'{outputDir}/transfer_meanFeatureImportanceDf.png', bbox_inches='tight')
    plt.cla()
    plt.clf()
    plt.close('all')
    std_auc = performance.loc[:, "auc"].std()
    avg_auc = performance.loc[:, "auc"].mean()
    # first = True
    error = [None] * (number_of_models)
    min_error = 999
    min_model_index = -1
    for i in range(0, number_of_models):
        error[i] = abs(performance.loc[i, "auc"] - avg_auc)
        if error[i] < min_error:
            min_error = error[i]
            min_model_index = i
    model_filename = outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(min_model_index) + ".h5"
    print("Loading the average model=" + model_filename)
    print(model_name+" average AUC="+str(avg_auc)+"  stdev="+str(std_auc))
    average_model = load_model(model_filename)
    return average_model, model_filename, avg_auc



def without_labels_source_training_guided_by_target_training(outputDir,
                                                             model_name, 
                                                             list_of_seeds, 
                                                             n_fold, 
                                                             source_filepath, 
                                                             source_size, 
                                                             target_filepath, 
                                                             target_size, 
                                                             adapt, 
                                                             num_quantile, 
                                                             list_of_vars, 
                                                             recalculate_cover,
                                                             recalculated_vars, 
                                                             not_adapted_vars, 
                                                             equal_width_vars,
                                                             group1_quantile, 
                                                             group2_quantile):

    # Just to get the column names
    features = [
        'term_36m'
        , 'term_60m'
        , 'grade_n'
        , 'sub_grade_n'

        , 'revol_util_n'
        , 'int_rate_n'
        , 'installment_n'
        , 'emp_length_n'

        , 'dti_n'
        , 'annual_inc_n'
        , 'loan_amnt_n'
        , 'cover'
        , 'Outcome'
    ]
    checkSelectedVars(list_of_vars, features)

    featureImportanceDf = pd.DataFrame(columns=features)

    number_of_models = len(list_of_seeds) * n_fold
    performance = pd.DataFrame(index=np.arange(0, number_of_models), columns=('repeat', 'iteration', 'training', 'test', 'auc'))
    repeat = 0
    for a_random_seed in list_of_seeds:
        np.random.seed(a_random_seed)

        #threshold = 0.02
        #source_extract = euclidean_extract(source_df, target_df, threshold)
        #source_sample = resample_without_replacement_df(source_extract, source_size,True)
        #source_data = normalise(source_sample)

        source_temp = pd.read_csv(source_filepath)
        source_temp = source_temp[features]
        source_sample = resample_without_replacement_df(source_temp, source_size, True)
        source_dataset, source_normalise_params_df = xnormalise(source_sample)

        target_temp = pd.read_csv(target_filepath)
        target_temp = target_temp[features]
        target_sample = resample_without_replacement_df(target_temp, target_size, True)
        target_dataset, target_normalise_params_df = xnormalise(target_sample)




        Xdata, Ydata = xy_split(target_dataset, 13)
        XYdata = target_dataset

        skf = StratifiedKFold(n_splits=n_fold, random_state=None, shuffle=False)
        #print("Number of splits = " + str(skf.get_n_splits(Xfull_target, Yfull_target, XYfull_target)))
        class_weights = weight_balance(Ydata)

        iteration = 0
        for train_index, test_index in skf.split(Xdata, Ydata, XYdata):

            # 2-define the empty list to store the layers
            dl_layer = [None] * (number_of_all_layers)
            # 3-define the number of nodes as an input for the first list (input function in keras)
            dl_layer[0] = Input(shape=(number_of_input,), name='NNinput')
            # 4- this comment connects the layers of NN to each other (if i is 1 means layer 1 connected to layer o and if i is 2 means layer 2 connected to 1 and so on.
            for i in range(1, number_of_layers + 1):
                dl_layer[i] = Dense(number_of_nodes, activation='relu', name='dense_function' + str(i))(dl_layer[i - 1])
            output = Dense(number_of_output, activation='sigmoid', name='NNoutput')(dl_layer[number_of_layers])
            one_model = Model(inputs=[dl_layer[0]], outputs=output)
            one_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            process1 = "model creation...model compile..."
            process3 = str(source_dataset.shape)
            # to split x and y for training
            XYtrain, XYtest = XYdata.iloc[train_index, :], XYdata.iloc[test_index, :]
            # use Ytrain here to extract source

            if adapt:
                # summary of target outcome
                df_target = deepcopy(XYtrain)
                len_target_training = len(df_target.index)
                if num_quantile == -1:
                    num_quantile = int(len_target_training/30)
                df_source_tmp2 = deepcopy(source_dataset)
                df_target_de = xdenormalise(df_target, target_normalise_params_df)
                df_source_de = xdenormalise(df_source_tmp2, source_normalise_params_df)
                # before_adapted = model_name+'_r'+str(repeat)+'_f'+str(iteration)+'_src_before_adapted.csv'
                # df_source_tmp.to_csv('./output_25feb2020/'+before_adapted)
                # target_use_to_adapt = model_name + '_r' + str(repeat) + '_f' + str(iteration) + '_tgt_use_to_adapt.csv'
                # df_target.to_csv('./output_25feb2020/' + target_use_to_adapt)
                for vname in list_of_vars:
                    #print('before=', df_source_de.head(5))
                    if vname in equal_width_vars:
                        #if vname == 'grade_n' or vname == 'sub_grade_n':
                        df_source_de = adaptation_equal_width(df_source_de, df_target_de, vname)
                    elif vname in recalculated_vars:
                        df_source_de = adaptation(df_source_de, df_target_de, vname, num_quantile, recalculate_cover)
                    elif vname in group1_quantile:
                        df_source_de = adaptation_only(df_source_de, df_target_de, vname, num_quantile)
                    elif vname in not_adapted_vars:
                        print('not adapted var=', vname)
                    #print('after=', df_source_de.head(5))
                    #source_sum_fname = './output_25feb2020/' + model_name + '_' + vname + '_r' + str(repeat) + '_f' + str(iteration) + 'source_sum.csv'
                    #target_sum_fname = './output_25feb2020/' + model_name + '_' + vname + '_r' + str(repeat) + '_f' + str(iteration) + 'target_sum.csv'
                    #source_sum_x_fname = './output_25feb2020/' + model_name + '_' + vname + '_r' + str(repeat) + '_f' + str(iteration) + 'source_sum_x.csv'
                    #source_sum.to_csv(source_sum_fname)
                    #target_sum.to_csv(target_sum_fname)
                    #source_sum_x.to_csv(source_sum_x_fname)
                    #df_source_tmp = adaptation(df_source_tmp, df_target, 'revol_util_n', num_quantile)
                # after_adapted = model_name + '_r' + str(repeat) + '_f' + str(iteration) + '_src_after_adapted.csv'
                # df_source_tmp.to_csv('./output_25feb2020/' + after_adapted)
                df_source_tmp, tmp_param_df = xnormalise(df_source_de)
            if adapt==False:
                XYtrain_src = deepcopy(source_dataset)
            else:
                XYtrain_src = deepcopy(df_source_tmp)

            Xtrain_src, Ytrain_src = xy_split(XYtrain_src, 13)
            Xtest_src, Ytest_src = xy_split(XYtrain_src, 13)  # note: using training as test

            # initial training using source data
            one_model.fit(Xtrain_src, Ytrain_src, epochs=50, batch_size=25, class_weight=class_weights, verbose=0)
            Ypred_src = (one_model.predict(Xtest_src)).flatten()

            #source_model = load_model(initial_model_file)
            one_model = make_last_layer_retrainable(one_model)
            process2 = "model transfer...model last layer made trainable..."

            # to split x and y for training
            XYtrain, XYtest = XYdata.iloc[train_index, :], XYdata.iloc[test_index, :]
            Xtrain, Xtest = Xdata.iloc[train_index, :], Xdata.iloc[test_index, :]
            Ytrain, Ytest = Ydata.iloc[train_index], Ydata.iloc[test_index]

            one_model.fit(Xtrain, Ytrain, epochs=50, batch_size=25, class_weight=class_weights, verbose=0)

            foldClassImportanceDf = ExplainerUtil.calcKerasFeatureImportance(one_model,
                                                                             Xtrain,
                                                                             Xtest,
                                                                             proportionOfTrainRowsToTrainWith=1.0,
                                                                             proportionOfTestRowsToAssessWith=1.0,
                                                                             plotSummaryFeatureImportance=True,
                                                                             optSummaryFeatureImportancePlotFile=f"{outputDir}/adapt_featureImportanceSummary-repeat-{repeat}-iteration-{iteration}.png")
            appendRowToDf(featureImportanceDf, foldClassImportanceDf.loc[0])

            Ypred = (one_model.predict(Xtest)).flatten()

            model_index = (repeat * n_fold) + iteration
            one_model.save(outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(model_index) + ".h5")
            performance.loc[model_index] = repeat, iteration, Ytrain.size, Ytest.size, calc_auc(Ypred, Ytest)
            print(process1 + process2 + "quantile="+ str(num_quantile) + " source= " + process3 + " repeat="+str(repeat)+"  Iteration="+str(iteration)+"   Train:", train_index.size, "   Test:", test_index.size, "   AUC:", calc_auc(Ypred, Ytest))
            iteration = iteration + 1
        repeat = repeat + 1


    dfToCsv(featureImportanceDf, f'{outputDir}/adapt_featureImportanceDf_transfer')
    # Clear plot axis & figures
    plt.cla()
    plt.clf()
    plt.close('all')
    meanFeatureImportanceDf = featureImportanceDf.mean().sort_values()
    dfToCsv(meanFeatureImportanceDf, f'{outputDir}/adapt_meanFeatureImportanceDf_transfer')
    meanFeatureImportanceDf.plot(kind='barh')
    plt.savefig(f'{outputDir}/adapt_meanFeatureImportanceDf_transfer.png', bbox_inches='tight')
    plt.cla()
    plt.clf()
    plt.close('all')

    std_auc = performance.loc[:, "auc"].std()
    avg_auc = performance.loc[:, "auc"].mean()
    # first = True
    error = [None] * (number_of_models)
    min_error = 999
    min_model_index = -1
    for i in range(0, number_of_models):
        error[i] = abs(performance.loc[i, "auc"] - avg_auc)
        if error[i] < min_error:
            min_error = error[i]
            min_model_index = i
    model_filename = outputDir + '/' + model_name +"_" + str(n_fold) + "_" + str(min_model_index) + ".h5"
    print("Loading the average model=" + model_filename)
    print(model_name+" average AUC="+str(avg_auc)+"  stdev="+str(std_auc))
    average_model = load_model(model_filename)
    return average_model, model_filename, avg_auc

def setupLogging(logLevel = logging.DEBUG):
    # Remove all handlers associated with the root logger object.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(format = '%(asctime)s %(filename)s:(%(lineno)d) %(levelname)s: %(message)s', level = logLevel)

DEF_SEED_COUNT = 2
DEF_FOLD_COUNT = 10
DEF_SRC_FILE = 'cd_07_11.csv'
DEF_TARGET_FILE = 'md_07_11.csv'
DEF_VAR_LIST = [
            #'term_36m'
            #, 'term_60m'
            #, 'grade_n'
            #, 'sub_grade_n'
        
            #, 'revol_util_n'
            #, 'int_rate_n'
            'installment_n'
            #, 'emp_length_n'
        
            #, 'dti_n'
            , 'annual_inc_n'
            , 'loan_amnt_n'
            , 'cover'
        ]

def usage(msg : Optional[str] = ''):
    print ('Usage: ' + sys.argv[0] + ' [-f foldCount] [-c seedCount] [-s sourceFilename] [-t targetFilename] [-v csvVarList]\n' +
           'Run ecmlpkdd2020 experiment.\n' +
           'Options:\n' +
           '-h print help and exit\n' +
           f'-f foldCount(default: {DEF_FOLD_COUNT})\n' +
           f'-c seedCount (default: {DEF_SEED_COUNT})\n' +
           f'-s sourceFilename (default: {DEF_SRC_FILE})\n' +
           f'-t targetFilename (default: {DEF_TARGET_FILE})\n' +
           f'-v csvVarList - A csv list of non-space-separated input features to be adapted (default: {DEF_VAR_LIST})\n' +
           f'{msg}\n')
    sys.exit(2)

def main():

    scriptDir, scriptFilename = os.path.split(os.path.abspath(__file__))
    
    try:
        #################
        # Init logging
        setupLogging(logLevel = logging.DEBUG)
        
        optlist, args = getopt.getopt(sys.argv[1:], 'hf:c:s:t:v:')  # 'sa:t:v:m:')
        # print >> sys.stdout, args
        # print >> sys.stdout, optlist

        # Default sett
        n_fold = DEF_FOLD_COUNT
        seedCount = DEF_SEED_COUNT
        inputDir : str = './input'
        datapath_source = f'{inputDir}/{DEF_SRC_FILE}'
        datapath_target = f'{inputDir}/{DEF_TARGET_FILE}'

        # list of vars to be adapted
        list_of_vars = DEF_VAR_LIST

        for o, a in optlist:
            if o == "-h":
                usage()
            elif o == "-c":
                seedCount = int(a)
            elif o == "-f":
                n_fold = int(a)
            elif o == "-s":
                datapath_source = a
            elif o == "-t":
                datapath_target = a
            elif o == "-v":
                list_of_vars = a.split(",")
            else:
                assert False, "unhandled option " + o

        outputDir : str = './output'
        os.makedirs(outputDir, exist_ok = True)

        # Initialisation
        #
        
        #adaptation strategy
        equal_width_vars = []
        recalculated_vars = []
        not_adapted_vars = ['revol_util_n', 'dti_n', 'grade_n', 'sub_grade_n', 'emp_length_n','term_36m', 'term_60m', 'cover']
        group1_quantile = ['installment_n', 'annual_inc_n', 'loan_amnt_n', 'cover'] # adapted with fix N quantile
        group2_quantile = []
        
        ProcessUtil.setAffinity(0)
        #setRandomSeed()
        recalculate_cover = False
        
        list_of_seeds = list(range(7, 7 + seedCount))
        #datapath_target = './input24feb2020/sb_07_11.csv'
        
        n_rows_source = 10000
        n_rows_target = 10000
        num_quantile = 10
        
        print("SOURCE="+datapath_source+" MAX SIZE="+str(n_rows_source))
        print("TARGET="+datapath_target)
        print("Recalculate cover="+str(recalculate_cover))
        print("Adapted features="+str(list_of_vars))
        print("Number of quantile="+str(num_quantile))
        print("Number of folds="+str(n_fold), "number of seeds="+str(len(list_of_seeds)))
        print("TARGET MODEL as a baseline benchmark")
        
        source_model, source_model_file, avg_source_auc = initial_training_source(outputDir,
                                                                                  "source_model", 
                                                                                  list_of_seeds, 
                                                                                  n_fold, 
                                                                                  datapath_source, 
                                                                                  n_rows_source)
        
        target_model, target_model_file, avg_target_auc = initial_training_target(outputDir,
                                                                                  "target_model", 
                                                                                  list_of_seeds, 
                                                                                  n_fold, 
                                                                                  datapath_target, 
                                                                                  n_rows_target)
        
        print("\nSOURCE transferred to TARGET, Source is transferred the same way as two experiments below, the only different is this one without adaptation of the source data")
        adapt=False
        trf_model, trf_model_file, avg_trf_auc = with_labels_source_training_guided_by_target_training(outputDir,
                                                                                                       "instance transfer", 
                                                                                                       list_of_seeds, 
                                                                                                       n_fold, 
                                                                                                       datapath_source, 
                                                                                                       n_rows_source, 
                                                                                                       datapath_target, 
                                                                                                       n_rows_target, 
                                                                                                       adapt, 
                                                                                                       num_quantile, 
                                                                                                       list_of_vars)
        
        print("\nAdapted(SOURCE) transferred to TARGET, source data is adapted based on target training within the fold")
        adapt=True
        adapt_model, adapt_model_file, avg_adapt_auc = without_labels_source_training_guided_by_target_training(outputDir,
                                                                                                                "Adaptive_transfer_v22", 
                                                                                                                list_of_seeds, 
                                                                                                                n_fold, 
                                                                                                                datapath_source, 
                                                                                                                n_rows_source, 
                                                                                                                datapath_target, 
                                                                                                                n_rows_target, 
                                                                                                                adapt, 
                                                                                                                num_quantile, 
                                                                                                                list_of_vars, 
                                                                                                                recalculate_cover,
                                                                                                                recalculated_vars, 
                                                                                                                not_adapted_vars, 
                                                                                                                equal_width_vars, 
                                                                                                                group1_quantile, 
                                                                                                                group2_quantile)
        print("\nTransfer only, improvement vs the baseline benchmark = ", (avg_trf_auc - avg_target_auc))
        
        print("\nAdaptation without label split, improvement vs transfer only = ", (avg_adapt_auc - avg_trf_auc))
        
    
        exit(0)

    except getopt.GetoptError as err:
        # print help information and exit:
        print (str(err))
        print (traceback.format_exc())
        sys.exit('Failed: %s' % (err))

    except Exception as e:
        print (traceback.format_exc())
        sys.exit('Failed: %s' % (e))


if __name__ == '__main__':
    main()

