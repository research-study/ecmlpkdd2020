'''
Created on 24Jun.,2019
'''

from enum import Enum, auto
from dataclasses import dataclass
from dataclasses_json import dataclass_json
from dataclasses_json.api import LetterCase
from typing import Optional, Dict, List, Any
import typing
import deprecated
from mashumaro.serializer.json import DataClassJSONMixin

class CsvColumnType(object):
# class CsvColumnType(str, Enum):
    STRING = 'STRING'
    FLOAT = 'FLOAT'
    INT = 'INT'
#     NUMERIC = 'NUMERIC'
    # TODO TEMPORAL
    # Will be auto one-hot transformed,
    # unless specified as isOrdinal
    CATEGORICAL = 'CATEGORICAL'

class CsvColumnState(object):
    # Base, final column
    BASE = 'BASE'
    # Can be derived into new column(s)
    DERIVABLE = 'DERIVABLE'
    # Has been derived from a derivable column
    DERIVED = 'DERIVED'


@dataclass_json(letter_case=LetterCase.CAMEL)  # now all fields are encoded/decoded from camelCase
@dataclass(order = True, unsafe_hash=True)
class CsvColumnMetadata(DataClassJSONMixin):
    '''
    CSV column metadata
    '''
    name : str
    csvColumnType : str
    csvColumnState : str = CsvColumnState.BASE
    description : str = ''
    isTrainingOutput : bool = False
    isOrdinal : bool = False
    # optValueList is for OneHot or Ordinal types
    # The OneHot names are name_<index+1>
    # The unknown value maps to name_-1, to match the
    # category_encoder behavior
    optValueList : Optional[List[str]] = None
    # Used to store the original CsvColumnMetadata for
    # columns that have been transformed, ex One-Hotted Columns
    # This is needed for when reverse transforms are needed
    # Note: Forward referenced types are specified via str rather than the bare type
    # See  "Forward references" section of PEP-0484
    optDerivedValue : str = None
#     optOriginalCsvColumnMetadata  = None
#     optOriginalCsvColumnMetadata : Optional['CsvColumnMetadata'] = None # type: ignore
#     optOriginalCsvColumnMetadata : Optional[typing.Any] = None # type: ignore

    def isOneHotDerivable(self):
        return self.csvColumnState == CsvColumnState.DERIVABLE and \
            self.csvColumnType == CsvColumnType.CATEGORICAL
    
    def isFactor(self):
        return self.csvColumnType == CsvColumnType.CATEGORICAL and self.isOrdinal
    
    def toResolvedIndex(self, unresolvedValue):
        '''
        Take an unresolvedValue (ex for ordinal types) and resolve it
        to the zero based index defined of the unresolvedValue in
        self.optValueList
        raises a ValueError if unresolvedValue is not found
        '''
        if not self.isOrdinal:
            return unresolvedValue
        if self.optValueList is None:
            raise Exception('{self.name} has no optValueList')
        return self.optValueList.index(unresolvedValue)

    def toUnResolvedValue(self, resolvedIndex):
        '''
        Reverse of toResolvedValue lookup
        raises an IndexError if resolvedIndex is out of bounds in self.optValueList
        '''
        if not self.isOrdinal:
            return resolvedIndex
        if self.optValueList is None:
            raise Exception('{self.name} has no optValueList')
        return self.optValueList[resolvedIndex]


def makeNewCsvColumnMetadata(obj,
            name : str = None,
             csvColumnType : str = None,
             csvColumnState : str = None,
             description : str = None,
             isTrainingOutput : bool = None,
             isOrdinal : bool = None,
             optValueList : Optional[List[str]] = None,
             optDerivedValue : str = None):
#              ,
#              optOriginalCsvColumnMetadata : Optional[CsvColumnMetadata] = None) :
    '''
    Builder pattern, incremental constructor - only changes fields not set to None
    '''
    return CsvColumnMetadata(
        name if not None else obj.name,
        csvColumnType if not None else obj.csvColumnType,
        csvColumnState = csvColumnState if not None else obj.csvColumnState,
        description = description if not None else obj.description,
        isTrainingOutput = isTrainingOutput if not None else obj.isTrainingOutput,
        isOrdinal = isOrdinal if not None else obj.isOrdinal,
        optValueList = optValueList if not None else obj.optValueList,
        optDerivedValue = optDerivedValue if not None else obj.optDerivedValue,
#         optOriginalCsvColumnMetadata = optOriginalCsvColumnMetadata if not None else obj.optOriginalCsvColumnMetadata,
        )
            
    return obj

