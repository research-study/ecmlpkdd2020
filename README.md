# ECMLPKDD2020 Experiment Readme

### For the paper: *'Building an accurate and transparent credit risk model using transfer learning and domain adaptation'*

Software Environment
-

Environment:

- Platform: Ubuntu/Kubuntu 18.04LTS (Virtual Machine or Native Host)
- Python 3.6.8+
- R 3.4.4+

Licencing
-
See LICENSE

The ShapSummaryPlot.py source file was adapted from
https://github.com/slundberg/shap/
Which has an MIT LICENSE

All other dependent libraries can be found in requirements.txt
And their respective licenses can be found at their respective web sites.

Environment Setup
-

### Install Prerequisites

All steps have can all be copied and pasted to a bash console

```bash
# Ensure python 3 is installed (for all users)
sudo apt-get install python3
sudo apt-get install python3-pip
sudo apt-get install python3-gdbm

# Install if you use Eclipse PyDev (for all users)
sudo apt-get install python3-tk

# For visualizing models
sudo apt-get install graphviz

# For using git
sudo apt install git

# For pip3 (Note: NOT sudo)
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py --user
python3 -m pip install --force-reinstall --user  "pip==19.3.1"

# create/edit this file with the contents below:
# File: ~/.pip/pip.conf

[global]
timeout = 480
```

## Virtual Environment Usage

The Python virtual environment being used is documented here:
<https://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html>

### Do the following once in your user account: 
```bash
# Set up virtual environment tools for the current user
echo "# Virtual Environment Wrapper"  >> ~/.bashrc
echo "export VIRTUALENVWRAPPER_PYTHON=`which python3`">> ~/.bashrc
# Set up base directory for all virtual environments
echo 'export WORKON_HOME=${HOME}/.pyvenv'>> ~/.bashrc
echo 'mkdir -p ${WORKON_HOME}'>> ~/.bashrc
echo 'source `which virtualenvwrapper.sh`' >> ~/.bashrc
export PATH=$HOME/.local/bin:$PATH

pip3 install --user "virtualenvwrapper==4.8.4"
. ~/.bashrc
```
**IMPORTANT: Now start a new terminal and continue in that** 

### Create Virtual Environment

```bash
# Only do ONCE for the virtual environment
# This will create the environment AND enter it
mkvirtualenv ecmlpkdd2020 -p python3
```

### Enter The Virtual Environment

*Do this **each time** you start a new bash shell and want to start
working in the virtual environment.*

```bash
# Activate the virtual environment
workon ecmlpkdd2020

# Note: When activated, the virtual environment will prefix
# the prompt with: (virtual environment), Ex:
(ecmlpkdd2020) \$
```

## Install Required Libraries

Data analysis libraries are ****installed inside the virtual
environment****, so that they can remain self contained, isolated &
stable - independent of any versions of python libraries that may be
installed at the system or user level. Specific versions are specified
to ensure the environment is reproducible.

```bash
# Install al required libraries
pip install -r requirements.txt
```

### Leave the virtual environment

*Do this each time you finish working in the virtual environment*

```bash
# Deactivate the virtual environment
deactivate
```

### Install R packages

```bash
# Only used to pre-process the data files for the experiment
# In a bash shell
sudo ./install-R-packages.sh stringr
```

### Clone the code/scripts
```bash
# Assume we clone to our home directory
cd

# clone ecmlpkdd2020 code
git clone git@gitlab.com:research-study/ecmlpkdd2020.git

# To use a specific  Digital Object Identifier (DOI)
git checkout <commit-sha>

```

### Experiment Directory Layout 

```bash
├──ecmlpkdd2020
│ ├──input \#Input zip, csv files
│ ├──output \# Generated output
```

### Download and prepare input data

Download and copy the following files and put them in the input/
directory:

<https://resources.lendingclub.com/LoanStats3a.csv.zip>
<https://resources.lendingclub.com/LoanStats3b.csv.zip>
<https://resources.lendingclub.com/LoanStats3c.csv.zip>
<https://resources.lendingclub.com/LoanStats3d.csv.zip>
<https://resources.lendingclub.com/LoanStats_2016Q1.csv.zip>
<https://resources.lendingclub.com/LoanStats_2016Q2.csv.zip>
<https://resources.lendingclub.com/LoanStats_2016Q3.csv.zip>
<https://resources.lendingclub.com/LoanStats_2016Q4.csv.zip>
<https://resources.lendingclub.com/LoanStats_2017Q1.csv.zip>
<https://resources.lendingclub.com/LoanStats_2017Q2.csv.zip>
<https://resources.lendingclub.com/LoanStats_2017Q3.csv.zip>
<https://resources.lendingclub.com/LoanStats_2017Q4.csv.zip>
<https://resources.lendingclub.com/LoanStats_2018Q1.csv.zip>
<https://resources.lendingclub.com/LoanStats_2018Q2.csv.zip>
<https://resources.lendingclub.com/LoanStats_2018Q3.csv.zip>
<https://resources.lendingclub.com/LoanStats_2018Q4.csv.zip>

### Run the following data preparation script:
```bash
cd ~/ecmlpkdd2020
./prepare-input-data.sh
```
### Run Modelling Scripts
```bash
# In a bash shell
workon ecmlpkdd2020
cd ~/ecmlpkdd2020

# Run the experiment with default options
# This will generate all model files, plots, etc in ./output
# the output on the console and in run.log
python3 ecml2020_domain_adaptation_auc.py 2>&1|tee run.log

# The experiment coode has a number of options, to obtain usage:
python3 ecml2020_domain_adaptation_auc.py  -h

Usage: ecml2020_domain_adaptation_auc.py [-f foldCount] [-c seedCount] [-s sourceFilename] [-t targetFilename] [-v csvVarList]
Run ecmlpkdd2020 experiment.
Options:
-h print help and exit
-f foldCount(default: 10)
-c seedCount (default: 2)
-s sourceFilename (default: cd_07_11.csv)
-t targetFilename (default: md_07_11.csv)
-v csvVarList - A csv list of non-space-separated input features to be adapted (default: ['installment_n', 'annual_inc_n', 'loan_amnt_n', 'cover'])

# Example option specification:
# Use 1 seed, 3 folds, adapt for 'annual_inc_n','loan_amnt_n'
python3 ecml2020_domain_adaptation_auc.py -c 1 -f 3 -v annual_inc_n,loan_amnt_n

# To clean the output, use the following command:
rm -f ./output/*
```